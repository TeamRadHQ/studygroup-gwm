<pre>
<?php
if ($_SERVER['HTTP_HOST'] !== 'teamradhq.com') {
	$app_css = '/css/app.css';
} else {
	$app_css = '/gwmstudy/css/app.css';
}
// var_dump( $_SERVER );
?>
</pre>
<?php 
function pageVars() {
	$page               = array();
	$page['path']       = $_SERVER['PHP_SELF'];
	$page['basename']   = basename($page['path']);
	$page['file']       = str_replace(['.php', '.html'], '', $page['basename']);
	$page['fullpath']   = str_replace($page['basename'], '', $page['path']);
	$page['folder']     = str_replace('/gwmstudy', '', $page['fullpath']);
	$page['folderName'] = str_replace('/', '', $page['folder']);
	return $page;
}
function pageLink() {
	$page = pageVars();
	if ($page['file'] === 'index') return;
	$link = '<a href="' . $page['basename'] . '">' . $page['file'] . '</a>';
	return $link;
}
function folderLink() {
	$page = pageVars();
	if($page['folder'] === '/') return;
	$link = '<a href="' . $page['fullpath'] . '">' . $page['folder'] . '</a>';
	return $link;
}
?>
<!DOCTYPE html>
<html>
	<head>
	 	<!-- Bootstrap -->
	 	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		
		<!-- Syntax Highlighting -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/styles/default.min.css">
		<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.4.0/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
		
		<!-- Site -->
		<title><?php echo $pageTitle;?> - GWM Study Group</title>
		<link rel="stylesheet" href="<?php echo $app_css; ?>">
	</head>

	<body>
	<header class="container">
		<h1> <?php echo $pageTitle;?> <small>GWM Study Group</small> </h1>
		<nav class="container">
				<a href="/gwmstudy/">GWM Study Group</a> ...
				<?php
				$folderLink = folderLink();
				if($folderLink) echo $folderLink, ' ... ';
				?>
				
				<?php
				$pageLink = pageLink();
				if($pageLink) echo $pageLink;
				?>
		</nav>
	</header>
	<hr>
	<main class="container">