<?php
/**
 * Takes the id, name and age of a student and returns an array.
 * @param  integer 	$id   The student's id.
 * @param  String 	$name The student's name.
 * @param  integer 	$age  The student's age in years.
 * @return array     	  An array containing the student's information.
 */
function student($id, $name, $age) {
	$student = array(
		'id' 	=> $id,
		'name' 	=> $name,
		'age' 	=> $age
	);
	return $student;
}
/**
 * Defines a new writing implement.
 * @param  string $color 	The colour it writes in.
 * @param  string $type  	The type of its kind [see pen() and pencil()]
 * @param  float  $size  	The size it writes in.
 * @return array        	An array describing a writing tool.
 */
function writing_tool($color, $type, $size) {
	return array(
		'color'	=>$color, 
		'type'	=>$type, 
		'size' 	=> $size
	);
}

/**
 * Defines a new pen array
 * @param  string $color 	The colour of this pen's ink.
 * @param  string $type  	The kind of pen: ballpoint, finepoint, sharpie, etc.
 * @param  float  $size  	The size of the pen tip.
 * @return array        	An array describing a pen.
 */
function pen($color='blue',$type='ballpoint', $size=0.4) {
	$pen = writing_tool($color, $type, $size);
	$pen['kind'] = 'pen';
	return $pen;
}
/**
 * Defines a new pencil array
 * @param  string $color 	The colour of this pencil's lead.
 * @param  string $type  	The kind of pen: ballpoint, finepoint, sharpie, etc.
 * @param  float  $size  	The size of the pen tip.
 * @return array        	An array describing a pen.
 */
function pencil($color='grey',$type='lead', $size='HB') {
	$pencil = writing_tool($color, $type, $size);
	$pencil['kind'] = 'pencil';
	return $pencil;
}

?>