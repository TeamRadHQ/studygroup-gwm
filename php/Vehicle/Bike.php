<?php
namespace Vehicle;
/**
 * This class defines a car. It contains Engine and Transmission 
 * objects. Thinking about the Car object: A car is a type of 
 * Vehicle so maybe we should create a vehicle class and extend it... 
 */
class Bike extends Vehicle{ 
	public function __construct($manufacturer='Malvern Star', $model='Shit Bike') {
		// Set BikeTransmission
		$this->transmission = new BikeTransmission;
		// Call parent contstructor.
		parent::__construct($manufacturer, $model);
	}
	/**
	 * Overrides the parent method with lower RPM and custom message.
	 * @param  integer $accelleration How much accelleration
	 * @return $this                  Returns self.
	 */
	public function accellerate($accelleration) {
		$gear = $this->transmission->gear();
		$this->engine->set_rpm( $accelleration * $gear );
		echo "<p>You're currently in gear <strong>".$this->transmission->gear()."</strong> and the you're pedalling your bike at <strong>" . $this->engine->rpm() . "</strong> RPM.</p>";
	}
}

?>
