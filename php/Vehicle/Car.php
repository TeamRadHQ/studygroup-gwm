<?php
namespace Vehicle;
/**
 * This class defines a car. It contains Engine and Transmission 
 * objects. Thinking about the Car object: A car is a type of 
 * Vehicle so maybe we should create a vehicle class and extend it... 
 */
class Car extends Vehicle{ 
	public function __construct($manufacturer='Nissan', $model='Skyline') {
		// Now set CarEngine and CarTransmission
		$this->engine = new CarEngine();
		$this->transmission = new CarTransmission;
		// Call parent contstructor.
		parent::__construct($manufacturer, $model);
	}
}
?>
