<?php
namespace Vehicle;
/**
 * This class defines a transmission for a Vehicle. 
 */
class BikeTransmission extends Transmission {
	/**
	 * An array of gears that the transmission has. 
	 * @var array
	 */
	protected $gears = array(1, 2, 3, 4, 5, 6);
	protected $frontGears = array(1, 2, 3);
	/**
	 * Overrides the parent method and sets gear according to
	 * the front gear and back gear. So if front gear is 3 and
	 * back gear is 6, then the gear is 18.
	 * @param integer $gear      The setting of the rear gear
	 * @param integer $frontGear The setting of the front gear.
	 */
	public function set_gear($gear=1, $frontGear=1) {
		// Check if $gear and $frontGear are valid and set gear.
		if( in_array($gear, $this->gears) 
			&& in_array($frontGear, $this->frontGears) ) {
			if ($frontGear > 1) {
				// Multiple frontGear by maximum gears.
				$frontGear = ($frontGear-1)* max($this->gears);
			} else {
				// else set to 0
				$frontGear = 0;
			}
			// Now add gear and frontGear
			$this->gear = $gear + $frontGear;
		} else {
			$this->gear = 1;
		}
		return $this;
	}
}
?>