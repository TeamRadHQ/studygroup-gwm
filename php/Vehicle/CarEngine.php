<?php
namespace Vehicle;
/**
 * This class defines an engine. It could be used in a number of different 
 * objects such as a Car or a Truck or Bus or Boat or Plane or any other kind 
 * of Vehicle. 
 */
class CarEngine extends Engine {
	/**
	 * The engine's capacity in liters.
	 * @var float
	 */
	protected $capacity;
	/**
	 * The engine's size.
	 * @var string
	 */
	protected $size;
	/**
	 * Sets the capcity, size and rpm.
	 * @param float   $capacity The engine's capacity in liters.
	 * @param string  $size     The engine's size.
	 * @param integer $rpm      The engine's current rpm.
	 */
	public function __construct($capacity=3.0,$size='V6',$rpm=0) {
		// Call parent construtor.
		parent::__construct($rpm);
		// Set capacity and size.
		$this->set_capacity($capacity);
		$this->set_size($size);
	}
	/**
	 * Sets the engine's capacity.
	 * @param float $capacity The engine's capacity in liters.
	 */
	public function set_capacity($capacity=3.0) {
		$this->capacity = $capacity;
	}
	/**
	 * Sets the engine's size.
	 * @param string $size The size of the engine - V6, V8 etc.
	 */
	public function set_size($size='V6') {
		$this->size = $size;
	}
}

?>