<?php
namespace Vehicle;
/**
 * This class defines an engine. It could be used in a number of different 
 * objects such as a Car or a Truck or Bus or Boat or Plane or any other kind 
 * of Vehicle. 
 */
class Engine {
	/**
	 * The engine's current rpm.
	 * @var int
	 */
	protected $rpm;
	/**
	 * Sets the rpm.
	 * @param integer $rpm      The engine's current rpm.
	 */
	public function __construct($rpm=0) {
		$this->set_rpm($rpm);
	}
	/**
	 * Sets the engine's speed in rpm. 
	 * @param int $rpm The engine's RPM.
	 */
	public function set_rpm($rpm=0) {
		$this->rpm = $rpm;
	}
	/**
	 * Gets the engine's current RPM
	 * @return integer The engine's current RPM
	 */
	public function rpm() {
		return $this->rpm;
	}
}

?>