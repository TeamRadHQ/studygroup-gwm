<?php
namespace Vehicle;
/**
 * This class defines a transmission for a Vehicle. 
 */
class CarTransmission extends Transmission {
	/**
	 * An array of gears that the transmission has. 
	 * @var array
	 */
	protected $gears = array('R', 'N', 1, 2, 3, 4, 5);

}
?>