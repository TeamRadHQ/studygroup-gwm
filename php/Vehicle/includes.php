<?php
// Parent Classes for Vehicle parts.
require_once('Engine.php');
require_once('Transmission.php');
// Parent Vehicle
require_once('Vehicle.php');
// Child classes for Vehicle parts.
require_once('BikeTransmission.php');
require_once('CarEngine.php');
require_once('CarTransmission.php');
// Child Vehicles
require_once('Bike.php');
require_once('Car.php');
?>