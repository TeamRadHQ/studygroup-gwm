<?php
namespace Vehicle;
require_once('Engine.php');
require_once('Transmission.php');

/**
 * This class defines a vehicle. It contains an engine and transmission.
 */
class Vehicle { 
	/**
	 * The car's manufacturer.
	 * @var string
	 */
	protected $manufacturer;

	/**
	 * The car's model.
	 * @var string
	 */
	protected $model;

	/**
	 * An instance of Transmission class.
	 * @var object Transmission
	 */
	public $transmission;

	/**
	 * An instance of Engine class.
	 * @var object Engine
	 */
	protected $engine;

	/**
	 * Constructs a new car with default transmission and engine.
	 * @param string $manufacturer The manufactuerer of the car.
	 * @param string $model        The model of the car.
	 */
	public function __construct($manufacturer='Nissan', $model='Skyline') {
		// Setup the engine and transmission if they're not already set.
		if( ! $this->engine )
			$this->engine = new Engine;
		if( ! $this->transmission )
			$this->transmission = new Transmission;
		$this->set_manufacturer($manufacturer);
		$this->set_model($model);
	}

	/**
	 * Sets the car's manufacturer.
	 * @param string $manufacturer The manufacturer of the car.
	 */
	public function set_manufacturer($manufacturer) {
		$this->manufacturer = $manufacturer;
	}

	/**
	 * Sets the car's model.
	 * @param string $model The model of the car.
	 */
	public function set_model($model) {
		$this->model = $model;
	}

	/**
	 * Checks Transmission's current gear and calculates Engine's RPM
	 * based on $acceleration.
	 * @param  integer $accelleration How much accelleration to apply.
	 * @return $this                Returns self.
	 */
	public function accellerate($accelleration) {
		// Get the current gear and set N = 0 and R = -1.
		$gear = $this->transmission->gear();
		switch($gear) {
			case 'R':
				$gear = -1;
				break;
			case 'N':
				$gear = 0;
				break;
		}
		// Let us say that engine RPM is equal to accelleration multipled 
		// by current gear multipled by 50.
		$this->engine->set_rpm( $accelleration * $gear * 50 );
		echo "<p>You're currently in gear <strong>".$this->transmission->gear()."</strong> and the engine is turning over at <strong>" . $this->engine->rpm() . "</strong> RPM.</p>";
		return $this;
	}
}

?>
