<?php
$pageTitle = "Vehicle Class & Extensions";
include('../includes/header.php');
// Include classes via include.php
require_once('includes.php')
?>
<h4>Car Class</h4>
<p> This class extends Vehicle. </p>
<pre>
<p>Call <strong>$car->transmission->set_gear()</strong> method. </p>
<p>Call <strong>$car->accelerate(60)</strong> method. </p>
<?php
$car = new Vehicle\Car;
$car->accellerate(60);
$car->transmission->set_gear(1);
$car->accellerate(60);
$car->transmission->set_gear(2);
$car->accellerate(60);
$car->transmission->set_gear(3);
$car->accellerate(60);
$car->transmission->set_gear(4);
$car->accellerate(60);
$car->transmission->set_gear(5);
$car->accellerate(60);
$car->transmission->set_gear(6);
$car->accellerate(60);
$car->transmission->set_gear('R');
$car->accellerate(60);
?>
</pre>

<h4>Bike Class</h4>
<p> This class extends Vehicle. </p>
<pre>
<p> Create a new instance of <strong>Bike</strong>. </p>
<p> Then loop through each of its 3 * 6 (18) gears and call <strong>$bike->accellerate()</strong> method.</p>
<?php
// Make an array for the bike's gears.
$gears = array(1,2,3,4,5,6);
$frontGears = array(1,2,3);
// Create a new bike.
$bike = new Vehicle\Bike;
// Now loop through each frontGear, set each rear 
// gear and call accellerate() method.
foreach( $frontGears as $frontGear ) {
	echo "<h5>Front Gear: $frontGear</h5>";
	// Loop through each gear
	foreach( $gears as $gear ) {
		// Set the bike's transmission to $gear, $frontGear
		$bike->transmission->set_gear($gear, $frontGear);
		// Call accellerate
		$bike->accellerate(50);
	}
	echo "<hr>";
}

?>
</pre>
<?php
include('../includes/footer.php');
?>