<?php
namespace Vehicle;
/**
 * This class defines a transmission for a Vehicle. 
 */
class Transmission {
	/**
	 * An array of gears that the transmission has. 
	 * @var array
	 */
	protected $gears = array();
	/**
	 * The current transmission's current gear setting.
	 * @var [type]
	 */
	protected $currentGear;	
	/**
	 * Sets the transmission to Neutral on construct.
	 */
	public function __construct() {
		$this->set_gear();
	}
	/**
	 * Sets the transmission to $gear if it's in $this->gears.
	 * @param string $gear The gear you want to set 
	 *                     the transmission to.
	 */
	public function set_gear($gear='N') {
		// Check passed $gear against this transmission's 
		// gears and set or neutral... 
		if(in_array($gear, $this->gears)) {
			$this->gear = $gear;
		} else {
			$this->gear = 'N';
		}
		return $this;
	}
	/**
	 * Returns the transmission's current gear.
	 * @return string The current gear setting.
	 */
	public function gear() {
		return $this->gear;
	}
}
?>