<?php
namespace GwmStudy;
// Add our header for bootstrap etc
$pageTitle = 'Classes';
include('./includes/header.php');
// Get our classes...
require_once('./GwmStudy/Student.php');
require_once('./GwmStudy/StudyGroup.php');
require_once('./GwmStudy/Subject.php');
$studyGroup = new StudyGroup('GWM','Diploma Web Development');
$studyGroup->addStudent( new Student(1234, 'Paul', 'paul.beynon@gmail.com') );
$studyGroup->addStudent( new Student(5678, 'Sacud', 'balla@gmail.com') );
$studyGroup->addStudent( new Student(9012, 'Pete', 'wizard@gmail.com') );
$studyGroup->addStudent( new Student(3456, 'Shehan', 'player@gmail.com') );

$student = new Student(3456, 'Nick', 19);
?>
<section class="col-sm-12">
	<h3> StudyGroup with Students </h3>
	<pre class="col-sm-6">
	<h4>$studyGroup->toArray() method</h4>
	<?php 
		print_r($studyGroup->toArray());
	?>
	</pre>
	<section class="col-sm-6">
	<h4>$studyGroup->table() method</h4>
	<?php echo $studyGroup->table() ?>
	</section>
</section>
<section class="col-sm-12">
<h3> StudyGroup with Subjects </h3>

<?php
$studyGroup->addSubject(new Subject('Designs for CMS', 'DCMS-0001', 'Tuesday', '13:00', 'Mel'));
$studyGroup->addSubject(new Subject('Web Database Connectivity B', 'WBDB-0001', 'Wednesday', '09:00', 'Mel'));
$studyGroup->addSubject(new Subject('Project Management for IT', 'PMIT-0001', 'Wednesday', '13:00', 'Graham'));
$studyGroup->addSubject(new Subject('Designs for CMS', 'DCMS-0002', 'Thursday', '13:00', 'Mel'));
$studyGroup->addSubject(new Subject('Web Trends', 'WTR-0001', 'Friday', '09:00', 'Mel'));
$studyGroup->addSubject(new Subject('Video Editing', 'VDED-0001', 'Friday', '14:00', 'Shane'));
?>
<pre class="col-xs-6">
<?php print_r($studyGroup->subjects()); ?>
</pre>
<section class="col-sm-6">
<h4>$studyGroup->table() method</h4>
<?php echo $studyGroup->table() ?>
</section>
</section>
<section class="col-sm-12">
<h3> Counts </h3>
<p> Group Size: <?php echo $studyGroup->studentCount(); ?>
<p> Number of Subjects: <?php echo $studyGroup->subjectCount(); ?>
</section>
<?php
include('./includes/footer.php');
?>
