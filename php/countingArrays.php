<?php
require_once('includes/functions.php');
$pageTitle = 'Counting Arrays';
include('includes/header.php');
?>
<p> Let's talk about my pencil case... </p>

<?php
$pencilcase = array();
$pencilcase['color'] = 'red';
$pencilcase['size'] = 'medium';
// Add contents array and put pens and pencils in it...
$pencilcase['contents'] = array();
$pencilcase['contents'][] = pencil();
$pencilcase['contents'][] = pencil('red', 'pastel','2B');
$pencilcase['contents'][] = pencil('blue', 'pastel','2B');
$pencilcase['contents'][] = pencil('yellow', 'pastel','2B');
$pencilcase['contents'][] = pencil('green', 'pastel','2B');
$pencilcase['contents'][] = pencil('grey', 'carbon','HB');
$pencilcase['contents'][] = pencil('grey', 'lead', '2B');
$pencilcase['contents'][] = pen();
$pencilcase['contents'][] = pen('blue','finepoint',0.1);
$pencilcase['contents'][] = pen('blue','sharpie',0.8);
$pencilcase['contents'][] = pen('red','finepoint',0.1);

?>
<pre><?php print_r($pencilcase);?></pre>

<p> I know, right? </p>

<p> We can easily calculate the total number of pens using the <code>count($array)</code> function. 

<pre>
count($pencilcase['contents']) = <?php echo count($pencilcase['contents']); ?>
</pre>

<p> Because this is a multidimensional array, it's a bit trickier to count how many pens or pencils we have. However, we can just use a <code>foreach()</code> loop to create a new array which just stores the kind of each item (pen or pencil):
<pre>
$kinds = array();
foreach($pencilcase['contents'] as $item) {
	$kinds[] = $item['kind'];
}
</pre>
<?php
$kinds = array();
foreach($pencilcase['contents'] as $item) {
	$kinds[] = $item['kind'];
}
?>
<p> This gives us an array of kinds:</p>
<pre> <?php print_r($kinds);?></pre>
<p> Now we can use <code>array_count_values</code> to count pens and pencils. This function will return an array with counts for each value it finds in the array. 
<pre>
$counts = array_count_values($kinds);
<?php 
print_r($counts=array_count_values($kinds))
?>
</pre>

<p> As you can see, this means there are <?php echo $counts['pen']; ?> pens and  <?php echo $counts['pencil']; ?> pencils in my pencil case... </p> 

<p> We could also do the same, but count the color of the pen or pencil instead:
<pre>
$kinds = array();
foreach($pencilcase['contents'] as $item) {
	$colors[] = $item['color'];
}
</pre>
<?php
$kinds = array();
foreach($pencilcase['contents'] as $item) {
	$colors[] = $item['color'];
}
?>
<p> This gives us an array of kinds:</p>
<pre> <?php print_r($colors);?></pre>
<p> Now we can use <code>array_count_values</code> to count the colors of our pens or pencils. 
<pre>
$counts = array_count_values($colors);
<?php 
print_r($counts=array_count_values($colors))
?>
</pre>

<p> So now we could use a <code>foreach()</code> loop. Only this time, let's use the array_keys() function to loop through each array key, instead of its contents:
<pre>
// Loop through each array key and output its colour and count.
foreach( array_keys($counts) as $key) {
	if ($counts[$key] >> 1 || $counts[$key] === 0) {
	// If the count is zero OR >1 then use plural
		echo "&lt;p style=\"color:$key\"&gt;There are $counts[$key] $key pens or pencils.&lt;/p&gt;";
	} else {
	// Else, only one means use singular
		echo "&lt;p style=\"color:$key\"&gt;There is $counts[$key] $key pen or pencil.&lt;/p&gt;";
	}
}
</pre>
<p> This will output the following:
<pre class="bg-success text-success" style="font-size:1.5em; font-weight:bold">
<strong>
<?php
// Loop through each array key and output its colour and count.
foreach( array_keys($counts) as $key ) {
	if ( $counts[$key] >> 1 || $counts[$key] === 0 ) {
	// If the count is zero OR >1 then use plural
		echo "<p style=\"color:$key\">There are $counts[$key] $key pens or pencils.</p>";
	} else {
	// Else, only one means use singular
		echo "<p style=\"color:$key\">There is $counts[$key] $key pen or pencil.</p>";
	}
}
?>
</strong>
</pre>
<?php
include('includes/footer.php');
?>