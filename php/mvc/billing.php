<?php
$pageTitle = "Handling Billing";

$include = '../includes/header.php';
include($include);
?>

<p class="lead">
	Billing is not really that difficult to implement if you have used the <a href="https://laravel.com/docs/5.2/eloquent" title="Eloquent ORM Documentation">Eloquent Object-relational Mapping</a>  in your project. It is simply a matter of defining your relationships in your models and then creating collections. 
</p>

<div class=" alert alert-success container">
	<p class="lead">
		<span class="glyphicon glyphicon-exclamation-sign col-xs-1"></span>
		<span class="col-xs-11"> 
            Some warning or something
        </span>
</div>






<?php
include('../includes/footer.php');
?>