<?php
$pageTitle = "Eloquent Relationships";

$include = '../includes/header.php';
include($include);
?>

<p class="lead">
If you have implemented <a href="https://laravel.com/docs/5.2/eloquent" title="Eloquent ORM Documentation">Eloquent Object-relational Mapping</a>  in your project, it's really easy to define relationships between one model and another.
</p>

<h3>Defining relationships</h3>
<p> Defining a relationship with between two models is simply a matter of creating a method in each model's class which returns data using the <code>$model->hasMany('Namespace\Class')</code> and <code>$model->belongsTo('Namespace\Class')</code>.</p>

<p> For example, if you wanted to create a one to many relationship between users and places - assuming that your models are namespaced as <code>App\Model</code>, you could do the following: </p>
<h4>User.php</h4>
<pre><code class="php">/**
 * Defines a one to many relationship between users and places.
 * @return  App\Model\Place
 */
public function places() {
	return $this->hasMany('App\Model\Place');
}
</code></pre>

<h4>Place.php</h4>
<pre><code class="php">/**
 * Defines a many to one relationship between places and users.
 * @return  App\Model\User
 */
public function user() {
    return $this->belongsTo('App\Model\User');
}
</code></pre>

<h3>Getting Records</h3>

<p> Once you've set up your relationships, getting the related records is really easy. </p> 
<p> For example, say you had a user with id 1 and you wanted to get ALL of their places and output their place names:</p>
<pre><code class="php">/* Get the user from the database */
$user = User::find(1);
/* Loop through each place and output its name */
echo "&ltul&gt;";
foreach ($user->places as $place) {
    echo "&ltli&gt;$place->name&ltli&gt;";
}
echo "&lt/ul&gt;";
</code></pre>

<p> So if they had five places, this code would output:

<div class=" alert alert-success container">
	<ul>
	<?php
	for($i = 0; $i < 5; $i++) {
		echo "<li>Place ".($i+1)." Name</li>";
	}
	?>
	</ul>
</div>

<div class=" alert alert-success container">
	<p class="lead">
		<span class="glyphicon glyphicon-exclamation-sign col-xs-1"></span>
		<span class="col-xs-11"> 
            Some warning or something
        </span>
</div>


<p> Now that you've set up your relationships, there are a lot of things you can do quite easily.</p>

<pre><code class="php">$user = User::find(1);
$user->places = Model\Place::where('user_id', '=', $user->id)
                           ->where('clicks_after_last_bill', '>', 100)
                           ->get();
</code></pre>



<?php
include('../includes/footer.php');
?>