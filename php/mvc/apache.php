<?php
$pageTitle = "Apache Configuration";

$include = './includes/header.php';
if (strpos($_SERVER['SCRIPT_FILENAME'], 'mvc')) {
    $include = '.' . $include;
}
include($include);
?>

<p class="lead">
	If you want to setup your Angular app to use data 
	from your top5 application, you will need to setup 
	virtual hosts for each installation. The easiest 
	way to implement this is to set each up to use the 
	same hostname, but use different ports. 
</p>

<div class=" alert alert-success container">
	<p class="lead">
		<span class="glyphicon glyphicon-exclamation-sign col-xs-1"></span>
		<span class="col-xs-11"> I strongly recommend attempting to connect your Angular app to your top5 project as this will halve the amount of work you need to do for both projects. </span>
</div>

<h3> Virtual Hosts </h3>

<p> Create a files in your project roots called <code>site.conf</code>.</p>

<h4>PHP MVC Framework <code>site.conf</code></h4>
<p> Make your MVC framework accessible on port <code>80</code>:</p>
<pre><code class="apache">&lt;VirtualHost top5.dev:80&gt;
    ServerName top5.dev
    DocumentRoot "/path/to/top5/slim/project/public"
    &lt;Directory "/path/to/top5/project/public"&gt;
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
    &lt;/Directory&gt;
&lt;/VirtualHost&gt;</code></pre>

<h4>Angular <code>site.conf</code></h4>
<p> Make your Angular installation accessible on port <code>8080</code>:</p>
<pre><code class="apache">&lt;VirtualHost top5.dev:8080&gt;
    ServerName top5.dev
    DocumentRoot "/path/to/top5/angular/project/"
    &lt;Directory "/path/to/top5/angular/project/"&gt;
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
    &lt;/Directory&gt;
&lt;/VirtualHost&gt;</code></pre>

<h4>Update XAMPP's Apache <code>httpd.conf</code> Configuration</h4>
<p> Open your XAMPP installation's <code>httpd.conf</code> file and do the following:
<pre><code class="apache">listen 80	# Find this line.
listen 8080	# Then add this line.</code></pre>

<p> Now add includes to your <code>site.conf</code> files:
<pre><code class="apache"># Top5 PHP
Include /path/to/top5/slim/project/site.conf

# Top5 Angular
Include /path/to/top5/angular/project/site.conf</code></pre>

<p> Finally, update your hosts file include your hostname:
<pre><code class="apache">127.0.0.1 	top5.dev</code></pre>

<h4>Restart Apache</h4>
<p>Then you should be able to access your apps by visiting these Url's:</p>
<ul>
	<li><code>http://top5.dev:90</code> - MVC Installation.</li>
	<li><code>http://top5.dev:8080</code> - Angular Installation.</li>
</ul>


<h4>If You Can't Edit Your Hosts File</h4>
<p> If you don't have access to your system's hosts file, instead, setup two alternative ports. </p>

<p> For example, setup your MVC controller to listen on port <code>90</code> and your Angular app to listen to port <code>8080</code>:</p>
<ul>
	<li><code>http://localhost:90</code> - MVC Installation.</li>
	<li><code>http://localhost:8080</code> - Angular Installation.</li>
</ul>





<?php
$include = './includes/footer.php';
if (strpos($_SERVER['SCRIPT_FILENAME'], 'mvc')) {
    $include = '.' . $include;
}
include($include);
?>