<?php
$pageTitle = "Drupal Stuff";
include('../php/includes/header.php');
?>

<h3> Modules </h3>

<p>These modules will provide additional functionality for content editing, customisation and ecommerce.</p>

<dl>
	pathauto
	<dt><h4>Content Editing</h4></dt>
	<dd>
		<p> These modules provide rich editing and media management. This allows you to reuse uploaded images in content and to format text etc.</p>
		<ul>
			<li><a href="https://www.drupal.org/project/wysiwyg">WYSIWYG</a> <br> What you see is what you get. Provides an interface for adding a text editor.</li>
			<li><a href="https://www.drupal.org/project/wysiwyg_ckeditor">CKEditor</a> <br> The core editor included in Drupal 8.</li>
		</ul>
	</dd>

	<dt><h4>Content Configuration</h4></dt>
	<dd>
		<p>These modules can be used to provide cusomisation and configuration of content.</p>
		<ul>
			<li><a href="https://www.drupal.org/project/views">Views</a> <br> Provides control over the display of custom sets of content </li>
			<li><a href="https://www.drupal.org/project/ddblock">Dynamic Display Block</a> <br> Slideshow functionality </li>
			<li><a href="https://www.drupal.org/project/Libraries">Required by ddblock</a> <br> Slideshow functionality </li>
			<li><a href="https://www.drupal.org/project/pathauto">Pathauto</a> <br> Adds automatic path generation for content types </li>
			<li><a href="https://www.drupal.org/project/token">Token</a> <br> Required by pathauto </li>
		</ul>
	</dd>

	<dt><h4>eCommerce</h4></dt>
	<dd>
		<p> These modules provide functionality for adding eCommerce functionality - products, shopping cart, ordering, etc.</p>
		<ul>
			<li><a href="https://www.drupal.org/project/commerce">Drupal Commerce</a> <br> Provides core functionality for eCommerce </li>
			<li><a href="https://www.drupal.org/project/ctools">Chaos Tools Suite</a> <br> Adds additional API funcitonality required by Drupal Commerce </li>
			<li><a href="https://www.drupal.org/project/Addressfield">Address Field</a> <br> Adds a data field for storing addresses </li>
			<li><a href="https://www.drupal.org/project/entity">Entity</a> <br> Required by commerce </li>
			<li><a href="https://www.drupal.org/project/rules">Rule</a> <br> Required by commerce </li>
		</ul>
	</dd>

</dl>

<!-- <h3>TinyMCE Installation Instructions</h3>
<p> In order to use TinyMCE you will need to install its JavaScript library. The documentation isn't very helpful, so I've added some step by steps.</p>
<ol>
	<li><a href="http://download.ephox.com/tinymce/community/tinymce_4.3.12.zip">Download TinyMCE</a></li>
	<li>Extract the archive and copy its contents into a new folder in the following location: <br> <code>sites/all/libraries/tinymce</code></li>
	<li>You will need to make some changes so that drupal can access the library at: <br> <code>sites/all/libraries/tinymce/jscripts/tiny_mce/tiny_mce.js</code> <br> 
		<br>
		<ol>
			<li>Extract the archive to the path above.</li>
			<li>Open the <code>tinymce</code> folder and rename the  folder <code>js</code>to <code>jscripts</code></li>
			<li>Open the <code>jscripts</code> folder and rename the folder <code>tinymce</code> to <code>tiny_mce</code></li>
			<li>Open the <code>jscripts</code> folder and rename the folder <code>tinymce</code> to <code>tiny_mce</code></li>
			<li>Open the <code>tiny_mce</code> folder and rename the file <code>tinymce.min.js</code> to <code>tiny_mce.js</code></li>
			<li>Finally open the file <code>tiny_mce.js</code> and replace the first line comment: <br>
				<code>// 4.3.12 (2016-05-10)</code> <br>
				with:<br>
				<code>// 4.3.12 (2016-05-10) majorVersion:"4",minorVersion:"3.12",releaseDate:"2016-05-10"</code>
			</li>
		</ol>
	</li>
</ol>
<p>Now you're done so go make a coffee or something...</p>

 -->



<?php
include('../php/includes/footer.php');
?>