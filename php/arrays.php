<?php
// Require our functions files to make life easier.
require_once('./includes/functions.php');

// Add our header for bootstrap etc
$pageTitle = 'Arrays';
include('./includes/header.php');
?>
<h3>Create an empty $class array</h3>
<p> We can create an empty array using <code>$class = array();</code> or <code>$class = [];</code></p>

<p> This will output the following:</p>

<?php
// Create an array for GWM class
$class = array();
// Print the array to the browser:
echo '<pre class="bg-success text-success">',print_r($class),'</pre><hr>';
?>
<h3>Add key/values to $class and create empty student array</h3>
<p> Now we can items using the syntax: <code>$class['key'] = 'value';</code></p>
<p> We can also add an empty array for students with: <code>$class['students'] = array();</code></p>
<?php
$class['group'] = 'GWM';
$class['course'] = 'Diploma Web Development';
$class['semester'] = 'Semester 1, 2016';

// Add an empty array to $class for adding students
$class['students'] = array();

// Print the array to the browser:
echo '<pre class="bg-success text-success">',print_r($class),'</pre><hr>';
?>
<h3>Populate $class['students'] with students.</h3>
<p> To make this easier, we should write a function which creates a student array.</p>
<pre><code class="php">function student($id, $name, $age) {
	return array(
		'id' => $id,
		'name => $name,
		'age' => $age
	);
}
</code></pre>

<p>Now we can use this to pass students into the array: <code>$class['students'][] = student(1234,'jimBob',21);</code></p>

<?php
// Now let's use our student($id, $name, $age) function 
// to add each student.
$class['students'][] = student(12456, 'BillyBob', 12);
$class['students'][] = student(57983, 'JimBob', 19);
$class['students'][] = student(39058, 'BobbyBob', 15);
$class['students'][] = student(20684, 'RustyBob', 13);

// Print the array to the browser:
echo '<pre class="bg-success text-success">',print_r($class),'</pre><hr>';

?>
<h3> Outputting the Array </h3>
<p> Now that we have an array full of information, we can output it to the user.</p>
<h4> Class Information </h4>
<p> The information about the class sits at the top level of the array so it's easy to output a key's value by calling it: <code>$class['key']</code>.</p> 
<table class="table">
	<thead>
		<th> Calling </th>
		<th> Outputs </th>
	</thead>
	<tr>
		<td> $class['group']</td>
		<td> <?php echo $class['group'];?></td>
	</tr>
	<tr>
		<td> $class['course']</td>
		<td> <?php echo $class['course'];?></td>
	</tr>
	<tr>
		<td> $class['semester']</td>
		<td> <?php echo $class['semester'];?></td>
	</tr>
</table>

<h4>Students</h4>
<p>The student array contains a collection of arrays which all contain the same keys. The only difference is their values. </p>
<p> We could manually target a student. For example calling <code>$class['students'][2]</code> returns:
<pre class="bg-success text-success"><?php print_r($class['students'][2]);?></pre>
<p>But it would be pretty boring to have to enter <code>$class['students'][0]['id'], $class['students'][1]['id'], $class['students'][2]['id']... </code> and so on. </p>
<p>Instead, we can use a foreach loop: <code>foreach($array as $item){}</code></p>
<p>This takes an array ($array) and loops through its items ($item).</p>
<p>So if we wanted to loop through each of our students and echo their student id:</p>
<pre><code class="php">
// Loop through each $student in the $class and echo their ID
foreach($class['students'] as $student) {
	echo $student['id'], '&lt;br&gt;';
}
</code></pre>
<table class="table">
	<thead>
		<th> ID </th>
		<th> Name </th>
		<th> Age </th>
	</thead>
<?php
// Loop through students and output a table row
foreach($class['students'] as $student) {
// Use Heredoc syntax for multi line output.
echo <<<EOT
<tr>
	<td>{$student['id']}</td>
	<td>{$student['name']}</td>
	<td>{$student['age']}</td>
</tr>
EOT;
} // End foreach loop...
?>
</table>
<?php
// Add our footer for bootstrap and jQuery
include('./includes/footer.php');

?>