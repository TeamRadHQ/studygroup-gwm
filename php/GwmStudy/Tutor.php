<?php
namespace GwmStudy;
require_once('Person.php');
class Tutor extends Person {
	/**
	 * Tutor ID
	 * @var integer
	 */
	protected $employeeId;
	/**
	 * Sets the id, name and email of the tutor.
	 * @param integer 	$employeeId   	The tutor's id.
	 * @param string 	$name 	The tutor's name.
	 * @param integer 	$email  	The tutor's email.
	 */
	public function __construct($employeeId, $name, $email) {
		parent::__construct($name, $email);
		$this->set_employeeId($employeeId);
		return true;
	}
	/**
	 * Returns the object's properties as an array.
	 * @return array An array containing the object's properties.
	 */
	public function toArray() {
		return array(
			'employeeId' => $this->employeeId(),
			'name' => $this->name(),
			'email' => $this->email()
		);
	}
	/**
	 * Sets the tutor's id
	 * @param integer 	$employeeId 	The tutor's id.
	 */
	public function set_employeeId($employeeId) {
		$this->employeeId = $employeeId;
		return true;
	}
	/**
	 * Sets the tutor's name
	 * @param string 	$name 	The name of the tutor.
	 */
	public function set_name($name) {
		$this->name = $name;
		return true;
	}
	/**
	 * Sets the tutor's email
	 * @param integer 	$email 	The tutor's email in years.
	 */
	public function set_email($email) {
		$this->email = $email;
		return true;
	}
	/**
	 * Gets the tutor's id
	 * @return integer 	The tutor's id.
	 */
	public function employeeId() {
		return $this->employeeId;
	}
	/**
	 * Gets the tutor's name
	 * @return string 	The tutor's name.
	 */
	public function name() {
		return $this->name;
	}
	/**
	 * Gets the tutor's email
	 * @return integer 	The tutor's email in years.
	 */
	public function email() {
		return $this->email;
	}
}
?>