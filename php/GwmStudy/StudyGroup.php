<?php
namespace GwmStudy;

class StudyGroup {
	protected $group;
	protected $course;
	protected $students;
	protected $subjects;
	public function __construct($group, $course) {
		$this->set_group($group);
		$this->set_course($course);
	}
	public function group() {
		return $this->group;
	}
	public function course() {
		return $this->course;
	}
	/**
	 * Returns an array containing students.
	 * @return array An array of student arrays.
	 */
	public function students() {
		$students = array();
		foreach($this->students as $student) {
			$students[] = $student->toArray();
		}
		return $students;
	}
	/**
	 * Returns an array containing subjects.
	 * @return array An array of subject arrays.
	 */
	public function subjects() {
		$subjects = array();
		foreach($this->subjects as $subject) {
			$subjects[] = $subject->toArray();
		}
		return $subjects;
	}
	/**
	 * Returns the object's properties as an array.
	 * @return array An array containing the object's properties.
	 */
	public function toArray() {
		return array(
			'group' => $this->group(),
			'course' => $this->course(),
			'students' => $this->students()
		);
	}
	public function set_group($group) {
		$this->group = $group;
		return true;
	}
	public function set_course($course) {
		$this->course = $course;
		return true;
	}

	public function addStudent(\GwmStudy\Student $student) {
		$this->students[] = $student;
	}
	
	public function addSubject(\GwmStudy\Subject $subject) {
		$this->subjects[] = $subject;
	}

	public function studentCount() {
		return count($this->students);
	}
	public function subjectCount() {
		return count($this->subjects);
	}

	public function table() {
		// Create table variable and add group and course.
		$table = <<<EOT
<table class="table">
	<thead>
		<th>{$this->group()}</th>
		<th colspan="2">{$this->course()}</th>
	</thead>
EOT;
		// Now loop through each student and output its tableRow
		if( ! empty($this->students) ) {
			foreach($this->students as $student) {
				$table .= $student->tableRow();
			}
		}
		// Now loop through each subject and output its tableRow
		if( ! empty($this->subjects) ) {
		$table .= <<<EOT
	<thead>
		<th>Code</th>
		<th>Subject</th>
		<th>Time</th>
		<th>Tutor</th>
	</thead>
EOT;
			foreach($this->subjects as $subject) {
				$table .= $subject->tableRow();
			}
		}
		// Now close off the table.
		$table .= "</table>";

		return $table;
	}
}
?>