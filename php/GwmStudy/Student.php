<?php
namespace GwmStudy;
require_once('Person.php');
class Student extends Person {
	/**
	 * Student id
	 * @var integer
	 */
	protected $studentId;
	/**
	 * Sets the studentId, name and email of the student.
	 * @param integer 	$studentId   	The student's id.
	 * @param string 	$name 	The student's name.
	 * @param integer 	$email  	The student's email.
	 */
	public function __construct($studentId, $name, $email) {
		parent::__construct($name, $email);
		$this->set_studentId($studentId);
		return true;
	}
	/**
	 * Returns the object's properties as an array.
	 * @return array An array containing the object's properties.
	 */
	public function toArray() {
		return array(
			'studentId' => $this->studentId(),
			'name' => $this->name(),
			'email' => $this->email()
		);
	}
	/**
	 * Sets the student's studentId
	 * @param integer 	$studentId 	The student's studentId.
	 */
	public function set_studentId($studentId) {
		$this->studentId = $studentId;
		return true;
	}
	/**
	 * Sets the student's name
	 * @param string 	$name 	The name of the student.
	 */
	public function set_name($name) {
		$this->name = $name;
		return true;
	}
	/**
	 * Sets the student's email
	 * @param integer 	$email 	The student's email in years.
	 */
	public function set_email($email) {
		$this->email = $email;
		return true;
	}
	/**
	 * Gets the student's studentId
	 * @return integer 	The student's studentId.
	 */
	public function studentId() {
		return $this->studentId;
	}
	/**
	 * Gets the student's name
	 * @return string 	The student's name.
	 */
	public function name() {
		return $this->name;
	}
	/**
	 * Gets the student's email
	 * @return integer 	The student's email in years.
	 */
	public function email() {
		return $this->email;
	}
}
?>