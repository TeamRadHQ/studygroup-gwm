<?php
namespace GwmStudy;
class Subject {

	protected $name;
	protected $code;
	protected $day;
	protected $time;
	protected $tutor;
	public function __construct($name, $code, $day, $time, $tutor) {
		$this->set_name($name);
		$this->set_code($code);
		$this->set_day($day);
		$this->set_time($time);
		$this->set_tutor($tutor);
	}
	public function name() {
		return $this->name;
	}
	public function code() {
		return $this->code;
	}
	public function day() {
		return $this->day;
	}
	public function time() {
		return $this->time;
	}
	public function tutor() {
		return $this->tutor;
	}
	function toArray() {
		return array(
			'name' => $this->name(),
			'code' => $this->code(),
			'day' => $this->day(),
			'time' => $this->time(),
			'tutor' => $this->tutor()
		);
	}
	public function set_name($name) {
		$this->name = $name;
	}
	public function set_code($code) {
		$this->code = $code;
	}
	public function set_day($day) {
		$this->day = $day;
	}
	public function set_time($time) {
		$this->time = $time;
	}
	public function set_tutor($tutor) {
		$this->tutor = $tutor;
	}
	public function tableRow() {
	return <<<EOT
<tr>
	<td>{$this->code()}</td>
	<td>{$this->name()}</td>
	<td>{$this->day()}, {$this->time}</td>
	<td>{$this->tutor()}</td>
</tr>
EOT;
	}
}

?>