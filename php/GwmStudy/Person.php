<?php
namespace GwmStudy;
class Person {
	/**
	 * person name
	 * @var String
	 */
	protected $name;
	/**
	 * person age
	 * @var integer
	 */
	protected $age;
	/**
	 * Sets the name and age of the person.
	 * @param string 	$name 	The person's name.
	 * @param integer 	$age  	The person's age.
	 */
	public function __construct($name, $email) {
		$this->set_name($name);
		$this->set_email($email);
		return true;
	}
	/**
	 * Returns the object's properties as an array.
	 * @return array An array containing the object's properties.
	 */
	public function toArray() {
		return array(
			'name' => $this->name(),
			'email' => $this->email()
		);
	}
	/**
	 * Sets the person's name
	 * @param string 	$name 	The name of the person.
	 */
	public function set_name($name) {
		$this->name = $name;
		return true;
	}
	/**
	 * Sets the person's email
	 * @param integer 	$email 	The person's email in years.
	 */
	public function set_email($email) {
		$this->email = $email;
		return true;
	}
	/**
	 * Gets the person's name
	 * @return string 	The person's name.
	 */
	public function name() {
		return $this->name;
	}
	/**
	 * Gets the person's email
	 * @return integer 	The person's email in years.
	 */
	public function email() {
		return $this->email;
	}
	public function tableRow() {
	return <<<EOT
<tr>
	<td colspan=2>{$this->name()}</td>
	<td><a href="mailto:{$this->email()}">{$this->email()}</a></td>
</tr>
EOT;
	}
}
?>