<?php
$pageTitle = "PHP";
include('./includes/header.php');
?>

<h3> Assessment Stuff </h3>
<ul>
	<li><a href="./mvc/apache.php">Top5 - Apache Configuration</a></li>
</ul>

<h3> Code Examples </h3>
<ul>
	<li><strong><a href="./arrays.php">Arrays</a></strong></li>
	<li><strong><a href="./arrays2.php">More Arrays</a></strong></li>
	<li><strong><a href="./GwmStudy/StudyGroup.php">Class: Study Group </a></strong></li>
	<li><strong><a href="./StudyGroup.php">Class: Car </a></strong></li>
	<li><a href="./Vehicle/">Vehicle Classes </a></li>
	<li><a href="./countingArrays.php">Counting Arrays</a></li>
	<li><a href="./classes.php">Classes</a></li>
</ul>
<p> * Items in <strong>bold</strong> have been covered during study group.</p>
<?php
include('./includes/footer.php');
?>