<pre>
<?php
require_once('Engine.php');
require_once('Transmission.php');

/**
 * This class defines a car. It contains Engine and Transmission 
 * objects. Thinking about the Car object: A car is a type of 
 * Vehicle so maybe we should create a vehicle class and extend it... 
 */
class Car { 
	/**
	 * The car's manufacturer.
	 * @var string
	 */
	protected $manufacturer;

	/**
	 * The car's model.
	 * @var string
	 */
	protected $model;

	/**
	 * An instance of Transmission class.
	 * @var object Transmission
	 */
	public $transmission;

	/**
	 * An instance of Engine class.
	 * @var object Engine
	 */
	protected $engine;

	/**
	 * Constructs a new car with default transmission and engine.
	 * @param string $manufacturer The manufactuerer of the car.
	 * @param string $model        The model of the car.
	 */
	public function __construct($manufacturer='Nissan', $model='Skyline') {
		$this->engine = new Engine;
		$this->transmission = new Transmission;
		$this->set_manufacturer($manufacturer);
		$this->set_model($model);
	}

	/**
	 * Sets the car's manufacturer.
	 * @param string $manufacturer The manufacturer of the car.
	 */
	public function set_manufacturer($manufacturer) {
		$this->manufacturer = $manufacturer;
	}

	/**
	 * Sets the car's model.
	 * @param string $model The model of the car.
	 */
	public function set_model($model) {
		$this->model = $model;
	}

	public function accellerate($accelleration) {
		// Get the current gear and set N = 0 and R = -1.
		$gear = $this->transmission->gear();
		switch($gear) {
			case 'R':
				$gear = -1;
				break;
			case 'N':
				$gear = 0;
				break;
		}
		// Let us say that engine RPM is equal to accelleration multipled 
		// by current gear multipled by 50.
		$this->engine->set_rpm( $accelleration * $gear * 50 );
		echo "<p>You're currently in gear <strong>".$this->transmission->gear()."</strong> and the engine is turning over at <strong>" . $this->engine->rpm() . "</strong> RPM.</p>";
	}
}
// GODZIRRA!!!!!
$godzilla = new Car('Nissan', 'Skyline');
// Floor it... 
$godzilla->accellerate(100);
// Idiot! Put the car in gear bruz...
$godzilla->transmission->set_gear(1);
// Now punch it... 
$godzilla->accellerate(100);
// Slide into second bro...
$godzilla->transmission->set_gear(2);
// DK!!!!!!!!!!!!!!!!!!!!!
$godzilla->accellerate(100);
// Now go backwards.
$godzilla->transmission->set_gear('R');
$godzilla->accellerate(100);
?>
</pre>