<?php

/**
 * This class defines a car transmission. Although, all Vehicles have 
 * transmissions so we might want to write Transmission as a more basic 
 * class then extend it with CarTransmission which is more specific to 
 * a Car.
 */
class Transmission {
	/**
	 * An array of gears that the transmission has. 
	 * @var array
	 */
	protected $gears = array('R', 'N', 1, 2, 3, 4, 5);
	/**
	 * The current transmission's current gear setting.
	 * @var [type]
	 */
	protected $currentGear;	
	/**
	 * Sets the transmission to Neutral on construct.
	 */
	public function __construct() {
		$this->set_gear();
	}
	/**
	 * Sets the transmission to $gear if it's in $this->gears.
	 * @param string $gear The gear you want to set 
	 *                     the transmission to.
	 */
	public function set_gear($gear='N') {
		// Check passed $gear against this transmission's 
		// gears and set it if it's valid. 
		if(in_array($gear, $this->gears)) {
			$this->gear = $gear;
		}
		return $this;
	}
	/**
	 * Returns the transmission's current gear.
	 * @return string The current gear setting.
	 */
	public function gear() {
		return $this->gear;
	}
}
?>