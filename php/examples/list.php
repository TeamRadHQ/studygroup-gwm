<?php
// Empty list items, then set
$list_items = array();
if(isset($_POST['list_items'])) {
	$list_items = $_POST['list_items'];
}

// Add new items 
if(isset($_POST['new_item'])) {
	if ($_POST['new_item']) $list_items[] = $_POST['new_item'];
}

// Delete Items
if(isset($_POST["delete"])) {
	// Loop through the list_items array
	foreach(array_keys($list_items) as $key) {
		print_r($key);
		if( in_array($list_items[$key], $_POST["delete"]) ) unset($list_items[$key]);
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>To Do List</title>
	<!-- Bootstrap -->
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<style>
		td, input[type=text], input[type=submit] {
			font-size: 2rem;
		}
		input[type=submit] {
			margin-top: 1em;
		}
		input[type=text],[type=submit] {
			padding-top: .75em;
			padding-bottom: .75em;
			height: 3em;
		}
	</style>
</head>
<body class="container">
	<h1>To Do List</h1>
	
	<form action="" method="post">
		<p><label for="task">Task:</label>
		<input class="form-control" type="text" name="new_item" placeholder="Add a new task...">
	<table>
		<tr>
			<td style="min-width:100px;"> <strong>Task</strong> </td>
			<td style="min-width:100px; text-align: center"> <strong>Remove</strong> </td>
		</tr>
		<?php
			foreach($list_items as $item) {
				echo "<tr><td>{$item}";
				echo '<input type="hidden" name="list_items[]" value="'.$item.'"></td>';
				echo '<td style="text-align:center"> <input type="checkbox" name="delete[]" value="' . $item . '"></td></tr>';
			}
		?>
	</table>
		<input type="submit" value="Update List" class="btn btn-block btn-primary"></p>
	</form>
	<script src="//code.jquery.com/jquery-2.2.2.js"></script>
	<script>
	$("input:text:visible:first").focus();

	</script>
</body>
</html>