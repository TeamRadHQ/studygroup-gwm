$(document).ready(function(){
	// Get the slideshow from the document.
	var $slideShow = $('#slideShow');
	// Hide all list items (our slides)
	$slideShow.children().hide();

	// Get the length of slideshow and start a counter.
	var length = $slideShow.children().length - 1, // 0 index
		counter = 0; // The current slide to display.
	

	/**
	 * This function cylces through each slide and displays it.
	 * @return {void}
	 */
	function slideShow() {
		// Send all slides to back
		$slideShow.children().css('z-index', 1);
		// Get the current slide
		$slideShow.children().eq(counter)
			.css('z-index', 2) 	// Bring slide to front
			.fadeToggle(1500)  	// Fade in over 1.5 seconds
			.delay(5000)   		// Wait for 4.5 seconds
			.fadeToggle(500); 	// Fade out over 0.5 seconds
		
		// Increment counter, or reset
		counter = (counter < length) ? counter + 1 : 0;

		// Set a timer to call self (show next slide)
		setTimeout(slideShow, 6000);
	}
	// Call slideShow() to begin showing slides.
	slideShow();
});