<?php
$pageTitle = "jQuery - SlideShow";
include('../php/includes/header.php');
?>

<p> See <a href="slideshow.htm">slideshow.htm</a> for slideshow example only.</p>

<ul id="slideShow">
	<li><span>1</span></li>
	<li><span>2</span></li>
	<li><span>3</span></li>
	<li><span>4</span></li>
	<li><span>5</span></li>
	<li><span>6</span></li>
</ul>

<section  class="alert alert-danger" role="alert">
	<h2><span class="glyphicon glyphicon-exclamation-sign"></span> Before You Start!</h2>
	<p> This script makes use of the jQuery JavaScript library. If you're unfamiliar with jQuery, I highly recommend you check out this video series:</p> <br>
	<p><a class="btn btn-primary btn-block" style="font-size:2rem" href="https://www.youtube.com/watch?v=Pt49y1gm0jw&list=PLqGj3iMvMa4KOekRWjjajinzlRK879Ksn">DevTips - jQuery in 15 minutes</a></p>
</section>

<section  class="alert alert-success" role="alert">
	<h2><span class="glyphicon glyphicon-exclamation-sign"></span> Sublime Text Users</h2>
	<p> Install the 'jQuery' bundle via Package Control: <code> ctrl + shift + p</code> > <code>Install + Enter</code> > <code>jQuery + Enter</code>.</p>
	
	<p> This package adds code completions for the jQuery API which can rapidly speed up your development. </p>
	<p><a class="btn btn-primary btn-block" style="font-size:2rem" href="https://packagecontrol.io/installation">Package Control Installation Instructions for Sublime</a></p>
</section>

<article>
	<h2>Description</h2>
	<p> This script creates a slideshow from a collection of HTML elements.</p>
	<p> The example above uses an unordered list. The script then cycles through each element, one after the other.</p>

	<h3>Variables</h3>
	<p> The script uses three variables. 
	<dl>
		<dt><dfn><code>$slideShow</code> <em>jQuery</em></dfn></dt>
		<dd>A jQuery collection containing the element with id <code>#slideShow</code>.</dd>
		<dt><dfn><code>length</code></dfn> <em>Integer</em></dt>
		<dd>Zero indexed length of the <code>$slideShow</code> collection. </dd>
		<dt><dfn><code>counter</code></dfn> <em>Integer</em></dt>
		<dd>A counter to indicate the current slide being shown. </dd>
	</dl>
<div class="row">
<section class="col-sm-5">
	<h3>Script Events</h3>
	<ol>
		<li> The entire script is wrapped in a jQuery <code>$(document).ready(function(){});</code> call. This syntax tells the browser to wait until the <em>entire</em> document is loaded before executing the code inside.</li>
		<li> Once the document is ready, the element with id <code>#slideShow</code> is stored in a jQuery collection called <code>$slideShow</code>. </li>
		<li> All of <code>$slideShow</code>'s children are hidden by calling <code>$slideShow.children().hide()</code>.</li>
		<li> The zero-indexed length of $slideShow is stored in variable <code>length</code>.<sup><a title="View Footnote" href="#1">1</a></sup> </li>
		<li> Variable <code>counter</code> is set to 0 to indicate the first slide in the collection.</li>
		<li> A new function called <code>slideShow()</code> is definied:
			<ol style="list-style-type:">
				<li> The <code>z-index</code> is set to 1 on all children of <code>$slideShow</code>.</li>
				<li> The current slide is selected by calling <code>$slideShow.children().eq(counter)</code>.</li>
				<li> An animation transition is defined lasting a total of 6.5 seconds:<sup><a title="View Footnote" href="#2">2</a></sup>
					<ol>
						<li> Its <code>z-index</code> is set to 2. This ensures that it will appear in front of any currently visible slides.</li>
						<li> The current slide is faded in over a period of 1.5 seconds. </li>
						<li> After 4.5 seconds, the slide is then faded out over a period of 0.5 seconds. </li>
					</ol>
				</li>
				<li> <code>counter</code> is then tested and incremented by 1, or reset to 0 if it is equal to <code>length</code>. </li>
				<li> Lastly, a 6 second timer is set and <code>slideShow()</code> calls itself. This triggers the display of the next slide.<sup><a title="View Footnote" href="#3">3</a></sup></li>
			</ol>
		</li>
		<li> Finally, <code>slideShow()</code> is called. This begins the slideshow. </li>
	</ol>
</section>
<section class="col-sm-7">
	<h3>Code</h3>
	<pre style="font-size:1.9rem"><code class="javascript">
$(document).ready(function(){
	
	
  $slideShow = $('#slideShow');


  $slideShow.children().hide();


  var length = $slideShow.children().length - 1,<sup><a title="View Footnote" href="#1">(1)</a></sup>  
      counter = 0; 


  function slideShow() {


    $slideShow.children().css('z-index', 1);


    $slideShow.children().eq(counter)<sup><a title="View Footnote" href="#2">(2)</a></sup> 
        .css('z-index', 2)
        .fadeIn(1500)
        .delay(4500)  
        .fadeOut(500); 


    counter = (counter < length) ? counter + 1 : 0;


    setTimeout(slideShow, 6000);<sup><a title="View Footnote" href="#2">(2)</a></sup> 
  }


  slideShow();

});
</code></pre>
</section>
</div>
	<h4>FootNotes</h4>
	<ol>
		<li> <a name="1"></a> Even though we can check <code>$slideShow.length</code>, I have stored its initial value in a variable beacuse it is not expected to change. This is a more efficient option because it's chepaer to query a static integer value than it is to call query <code>$slideShow.length</code> which would require the browser to count the length <em>every time</em>. </li>
		<li> <a name="2"></a> When scripting your slideshow, note the total time of your animation. This is important because <code>$slideShow.eq(counter).fadeIn().delay().fadeOut()</code>, the interpreter does not wait before continuing to execute the rest of the script. </li>
		<li> <a name="1"></a> If a timer is not set, then the slide animations will continuously fire until the browser overloads and crashes! </li>
		<li> <a name="1"></a> Also note that the timer is set to be less than the total duration of the slide animation (slide = 6.5 sec, timer=6 sec). This ensures a smoother transition as the new slide will fade in over the other. If the timer duration is longer, or the same length as the slide transition, the user will see a blank screen between each slide.</li>
	</ol>
	<h2>References</h2>
	<h3>JavaScript</h3>
	<ul>
		<li>
			<code><a title="View Method Documentation" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/length">Array.length</a></code>
			<br>Gets the number of items contained in <code>Array</code>.
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Conditional_Operator">condition ? expr1 : expr2</a></code>
			<br>Conditional (ternary) operator, short-hand for 
			<code>
				if(condition) {
					some value;
				} else { 
					some other value;
				} 
			</code>
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout">setTimeout(callBack(), time)</a></code>
			<br>Calls a function after <code>time</code>.
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions">function name(arguments) { expression; return something; }</a></code>
			<br> Defines a function called <code>name</code> which accepts <code>arguments</code>, performs <code>expression</code> and returns <code>something</code>.
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#Function_expressions">function(arguments){ expression; return something; }</a></code>
			<br> A function expression, or anonymous function, which accepts <code>arguments</code>, performs <code>expression</code> and returns <code>something</code>.
		</li>
	</ul>
	<h3>jQuery</h3>
	<ul>
		<li>
			<code><a title="View Method Documentation" href="https://api.jquery.com/ready/">$(document).ready(function(){})</a></code> 
			<br>Wait until the DOM is loaded and then call <code>function()</code>. 
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://api.jquery.com/category/selectors/">$([selector])</a></code>
			<br>Returns a jQuery collection of all elements having valid CSS <code>selector</code>. 
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://api.jquery.com/children/">jQuery.children([selector])</a></code>
			<br> Searches a jQuery collection for any child elements matching <code>selector</code> and returns in a new jQuery collection. 
			<br> If no <code>selector</code> is supplied the collection contains ALL children from the original collection.
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://api.jquery.com/eq/">jQuery.eq(index)</a></code> 
			<br> Select the element at <code>index</code> in the jQuery collection.
		</li>
		<li>
			<code><a title="View Method Documentation" href="https://api.jquery.com/css/">jQuery.css( propertyName, value )</a></code>
			<br> Update the value of CSS <code>propertyName</code> to <code>value</code>.
		</li>
		<li>
			<code><a title="View Method Documentation" href="http://api.jquery.com/fadein/">jQuery.fadeIn(duration)</a></code>, <code><a title="View Method Documentation" href="http://api.jquery.com/fadeout/">jQuery.fadeOut(duration)</a></code> and  <code><a title="View Method Documentation" href="http://api.jquery.com/fadetoggle/">jQuery.fadeToggle(duration)</a></code>
			<br> Show or hide the elements in the jQuery collection with a fade animation which last for <code>duration</code> milliseconds.
		</li>
		<li>
			<code><a title="View Method Documentation" href="http://api.jquery.com/fadeout/">jQuery.delay(duration)</a></code>
			<br> Sets a timer to delay subsequent items in the queue by <code>duration</code> milliseconds..
		</li>
	</ul>
</article>
<script id="hack">
$(document).ready(function(){
	$('head').append('<link rel="stylesheet" href="css/slideshow.css">');
	$('body').append('<script src="js/slideshow.js">');
});
</script>
<?php
include('../php/includes/footer.php');
?>