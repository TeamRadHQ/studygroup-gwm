<?php

function listBox($className, $count=6) {
	$listBox = '<ul class="'.$className.'">';
	for($ii = 0; $ii < $count; $ii++) {
		$listBox.="<li>". ($ii+1) ."</li>";
	}
	$listBox.="</ul>";
	echo $listBox;
}
?>
<?php
$pageTitle = "CSS FlexBox";
include('../php/includes/header.php');
?>

<p> View page source for inline stylesheet. </p>

<style>
	main {
		font-size: 2rem;
	}
	main {
		box-sizing: border-box;
		display: flex;
		flex-direction: column;
		background:#eee;
		margin-top: 1rem;
	}

	main > header { 
		border: 10px solid green;
		order: 1;
	}
	main > nav {
		box-sizing: border-box;
		order: 2;
		border: 10px solid navy;
		margin: 0;
		padding: 0;
	}
	main > article {
		box-sizing: border-box;
		border: 10px solid red;
		order: 3;
		display: flex;
		/*flex-wrap: wrap;*/
		flex-direction: row;
	}
	main > nav > ul {
		list-style-type: none;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: space-between;
		margin:0;
		padding:0;
	}
	main > nav > ul > li {
		margin:0;
		padding:0;
		border: 10px solid blue;
		flex-grow: 1;
		text-align: center;
	}
	article > section {
		box-sizing: border-box;
		border: 10px solid hotPink;
		background: pink;
	}
	article > aside {
		flex-shrink: 1;
		border: 10px solid skyBlue;
		background: #cff;
		max-width: 33%;
	}
	@media (max-width: 768px) {
		main > article {
			flex-direction: column;
		}
		article > aside {
			max-width: 100%;
		}
		main > header {
			order: 1;
		}
		main > article {
			order: 2;
		}
		main > nav > ul {
			order: 3;
			flex-direction: column;
		}
	}
	main > header, 
	main > article > section, 
	main > article > aside, 
	main > nav > ul >li {
		padding: 0.5rem;
	}
</style>
<main>
	<article>
		<section>
			<h2>Article Content</h2>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet vero numquam omnis facere. Repudiandae, itaque. Quibusdam aspernatur quo, tempore nulla nisi. Quidem quia saepe tempore, minima sapiente animi libero dolor?</p>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet vero numquam omnis facere. Repudiandae, itaque. Quibusdam aspernatur quo, tempore nulla nisi. Quidem quia saepe tempore, minima sapiente animi libero dolor?</p>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet vero numquam omnis facere. Repudiandae, itaque. Quibusdam aspernatur quo, tempore nulla nisi. Quidem quia saepe tempore, minima sapiente animi libero dolor?</p>
		</section>
		<aside>
			<h3>Article Aside</h3>
			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet vero numquam omnis facere. Repudiandae, itaque. Quibusdam aspernatur quo, tempore nulla nisi. Quidem quia saepe tempore, minima sapiente animi libero dolor?</p>
			
		</aside>
	</article>
	<header>
		<h1> Page Header </h1>
	</header>
	<nav>
		<ul>
			<li>Nav 1</li>
			<li>Nav 2</li>
			<li>Nav 3</li>
			<li>Nav 4</li>
			<li>Nav 5</li>
			<li>Nav 6</li>
		</ul>
	</nav>
</main>

<?php
include('../php/includes/footer.php');
?>