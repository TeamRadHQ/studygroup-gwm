# README #

This is the repository for code examples for the GWM Web Development Study Group.

## View Source

This repository contains example source code. You can either clone it to your computer and run it on our own XAMPP server. Or you can view it online:

http://teamradhq.com/gwmstudy/

### Tutorials ###

* [Codecourse - Authentication with Slim 3](https://www.youtube.com/playlist?list=PLfdtiltiRHWGc_yY90XRdq6mRww042aEC)
    - How to setup user authentication.
    - Introduction to Twig templates.


* [LevelUpTuts - Angular JS For Everyone](https://www.youtube.com/watch?v=NJ4FYsRV3nU&list=PLLnpHn493BHF6utwkwpo7RN-GPg1sZhvK)
    - Introduction to creating templates and outputting data.
    - Does not cover external data sources.

### What does this study group cover? ###

* PHP
    - [WordPress Theme Functions](https://bitbucket.org/TeamRadHQ/studygroup-gwm/wiki/php/WPThemeFunctions.md) *(wiki)*
* HTML & CSS 
* JavaScript

### How do I get set up? ###

You can download this repository to get all of the most up-to-date code examples. Additionally, you can view the source code in the browser.

# CSS Cheat Sheets

* [Fonts](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-01-Fonts-A4.pdf)

* [Lengths](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-02-Length-A3.pdf)

* [The Box Model](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-03-Box.Model-A3.pdf)

* [Backgrounds](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-04-Background-A4.pdf)

* [Borders](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-05-Borders-A3.pdf)

* [CSS Selectors](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-06-Selectors-A3.pdf)

* [CSS Positioning](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-07-Positioning-A3.pdf)

* [CSS Pseudo Classes](https://bytebucket.org/TeamRadHQ/studygroup-gwm/wiki/Assets/CSS.Cheat.Sheet-08-Pseudo.Classes-A3.pdf)

### Watch this if you want to roll with the big boys... and girls... 

[DevTips - GitHub for Noobs](https://www.youtube.com/playlist?list=PLqGj3iMvMa4LFz8DZ0t-89twnelpT4Ilw)

A good introduction to using git. 


[CodeCasts.tv - Version Control with Git](https://www.youtube.com/watch?v=uTF03VXiuGg&list=PLq0VzNtDZbe9QLq8YCizFN2TVWvlLjrvX)

After watching GitHub for noobs, this series goes deeper into version control with git. 

[LevelUpTuts - Sublime Text Tutorial](https://www.youtube.com/playlist?list=PLLnpHn493BHEYF4EX3sAhVG2rTqCvLnsP)

This series covers all of the basics of Sublime text including editing, package control and project management. 